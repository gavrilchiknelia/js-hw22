let button = document.getElementById('button');

function clickButton() {

    let input = document.createElement('input');
    document.body.insertBefore(input, document.body.firstChild );
    let btn2 = document.createElement('button');
    btn2.innerHTML = 'Нарисовать';
    document.body.insertBefore(btn2, document.body.children[1]);
    button.remove();

    function btn2Click() {

        let n = 100;
        if (input.value > 0) {
            let wrap = document.createElement('div');
            wrap.setAttribute('id', 'wrapp');
            document.body.insertBefore(wrap, document.body.children[2]);
            for (let i = 0 ; i <= n; i++ ) {
                let ring = document.createElement('div');
                ring.style.display = "inline-block";
                ring.style.width = `${input.value + "px"}`;
                ring.style.height = `${input.value + "px"}`;
                ring.style.boxSizing = 'border-box';
                ring.style.border = '5px solid green';
                ring.classList.add("rings");
                ring.style.borderRadius = '50%';
                wrap.appendChild(ring);
            }
            input.value = '';

        } else {
            alert('Enter correct value')
        }
    }
    btn2.onclick = btn2Click;
}
button.onclick = clickButton;

function hide(event) {
    if (event.target.classList.contains('rings')) {
        $(event.target).fadeOut(100);
    }
}

$('body').click(hide);
